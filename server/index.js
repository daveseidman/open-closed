const { Client } = require('@googlemaps/google-maps-services-js');
const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
// const path = require('path');
const { key } = require('../private/google-maps-creds.json');

const app = express();
app.use(bodyParser.json());

const client = new Client({});

const locationsFile = `${__dirname}/places.json`;
let _savedLocations = null;

const maxPages = 10;
const radius = 1000;
const type = 'any';

const pad = (number) => {
  let string = number > 0 ? '+' : '-';
  if (Math.abs(number) < 100) string += '0';
  if (Math.abs(number) < 10) string += '0';
  string += Math.abs(number);
  if (string.indexOf('.') === -1) string += '.';
  while (string.length <= 7) {
    string += '0';
  }
  return string.substring(0, 8);
};

const formatLocation = location => `${pad(location.lat)}_${pad(location.lng)}`;

// find all places near location and then attach hours to each
const search = location => new Promise((resolveAll) => {
  // get a list of places near provided location
  const getPlaces = () => new Promise((resolve) => {
    // use recursion to iterate through pagination returned from google maps api
    const getPlacesRecursive = (count, places, pageToken) => {
      client.placesNearby({ params: { location, radius, type, key, pageToken } }).then(({ data }) => {
        const { results, next_page_token } = data;
        places = places.concat(results.map(res => ({ id: res.place_id, location: res.geometry.location })));
        console.log(`page ${count}: found ${places.length} places`);
        if (count >= maxPages) return resolve(places);
        if (!next_page_token) return resolve(places);
        getPlacesRecursive(count + 1, places, next_page_token);
      }).catch(err => console.log('ERROR', err));
    };
    getPlacesRecursive(0, [], null);
  });

  // get the hours for one place
  const getHours = place => new Promise((resolve) => {
    client.placeDetails({
      params: {
        place_id: place.id,
        key,
      },
      fields: ['name', 'open_hours', 'location'],
    }).then(({ data }) => {
      const { name, place_id, opening_hours } = data.result;
      if (!opening_hours) return resolve();
      if (!opening_hours.periods) return resolve();
      return resolve({ name, place_id, hours: opening_hours.periods, location: place.location });
    });
  });

  // get the hours for all places
  const getAllHours = places => new Promise((resolve) => {
    const promises = [];

    places.forEach((place) => { promises.push(getHours(place)); });

    Promise.all(promises).then((res) => {
      const filtered = res.filter(item => item);
      // _places = filtered;
      console.log('found the hours for ', filtered.length, 'places');
      resolve(filtered);
    });
  });

  // get all places, then get their hours, then write them to file and return
  getPlaces().then(getAllHours).then((places) => {
    _savedLocations[formatLocation(location)] = { places, date: new Date().toString() };
    fs.writeFile(locationsFile, JSON.stringify(_savedLocations), ((err) => {
      console.log('error writing?', err);
      return resolveAll(places);
    }));
  });
});

app.listen('8000', () => {
  console.log('open-close server listening on port 8000');
});

app.post('/api/search/', (req, res) => {
  console.log('requesting places for', req.body.location);
  if (req.body.location) {
    fs.readFile(locationsFile, (err, data) => {
      const savedLocations = JSON.parse(data);
      _savedLocations = savedLocations; // store in memory
      const index = Object.keys(savedLocations).indexOf(formatLocation(req.body.location));
      if (index >= 0) {
        console.log('location already crawled on', savedLocations[formatLocation(req.body.location)].date);
        const location = savedLocations[Object.keys(savedLocations)[index]];
        res.send(location.places);
      } else {
        search(req.body.location).then((places) => {
          res.send(places);
        });
      }
    });
  } else {
    res.send({ error: 'no location provided' });
  }
});
