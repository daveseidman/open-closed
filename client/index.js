import autoBind from 'auto-bind';
import onChange from 'on-change';
import { addEl, createEl } from 'lmnt';
import { Raycaster, Fog, LinearFilter, FrontSide, Scene, PerspectiveCamera, WebGLRenderer, SphereGeometry, Mesh, AmbientLight, DirectionalLight, MeshBasicMaterial, MeshNormalMaterial, Vector2, Vector3, Spherical } from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import './index.scss';

const wireframeMaterial = new MeshBasicMaterial({ wireframe: true, side: FrontSide });
const cities = {
  newyork: { lat: 40.706197, lng: -73.9960381 },
  boston: { lat: 42.3560598, lng: -71.136622 },
  providence: { lat: 41.8507903, lng: -71.4752807 },
  charleston: { lat: 32.7699473, lng: -79.9620748 },
  jacksonville: { lat: 30.3611156, lng: -81.8189392 },
};
const radius = 1;

const positionFromLocation = ({ lat, lng }) => {
  const phi = (90 - lat) * (Math.PI / 180);
  const theta = (lng + 180) * (Math.PI / 180);

  const x = -(radius * Math.sin(phi) * Math.cos(theta));
  const z = (radius * Math.sin(phi) * Math.sin(theta));
  const y = (radius * Math.cos(phi));

  return new Vector3(x, y, z);
};

// const locationFromPosition = (position) => {
//   position.normalize();
//   let lng = -Math.atan2(-position.z, -position.x) - Math.PI;
//   if (lng < -Math.PI) lng -= Math.PI * 2;
//   const p = new Vector3(position.x, 0, position.z);
//   p.normalize();
//   let lat = Math.acos(p.dot(position));
//   if (position.y < 0) lat *= -1;
//   // lng *= 90;
//   return { lat, lng };
// };

class App {
  constructor() {
    autoBind(this);
    this.el = createEl('div', { className: 'app' });


    this.scene = new Scene();
    this.renderer = new WebGLRenderer({ antialias: true });
    this.camera = new PerspectiveCamera(60, this.width / this.height, 0.01, 4);

    this.camera.position.z = 2;

    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.controls.enablePan = false;
    this.controls.enableDamping = true;
    this.controls.enableZoom = true;
    this.controls.minDistance = 1.01;
    this.controls.maxDistance = 2;
    this.controls.rotateSpeed = 0.1;
    this.controls.zoomSpeed = 0.1;

    this.mouse = new Vector2();
    this.raycaster = new Raycaster();

    addEl(this.el, this.renderer.domElement);
    new GLTFLoader().load('assets/models/earth6.glb', (res) => {
      console.log('loaded');
      // res.scene.getObjectByName('earth-old').visible = false;
      // res.scene.getObjectByName('black').visible = false;
      // res.scene.getObjectByName('land').visible = false;
      this.globe = res.scene.getObjectByName('earth');

      // this.globe.material.emissiveIntensity = 20;
      this.globe.material.map.minFilter = LinearFilter;
      this.globe.material.map.magFilter = LinearFilter;
      this.globe.material.map.anisotropy = this.renderer.capabilities.getMaxAnisotropy();
      // this.mat = this.globe.material;

      // console.log(this.globe.material);
      // this.globe.material = new MeshBasicMaterial({ color: 0xdddddd, map: this.mat.map });
      // this.globe.material.wireframe = true;
      // this.land = res.scene.getObjectByName('land');
      // this.globe.material = wireframeMaterial;
      // this.land.material = wireframeMaterial;
      // res.scene.traverse((obj) => {
      //   if (obj.material) obj.material = wireframeMaterial;
      // });
      this.scene.add(this.globe);
    });

    this.ambient = new AmbientLight(0xffffff, 2);
    this.scene.add(this.ambient);

    // this.scene.fog = new Fog(0x000000, 1, 2.5);
    // this.scene.
    // this.sun = new DirectionalLight(0xffffee, 2);
    // this.sun.target.x = 2;
    // this.sun.target.z = 1;
    // this.scene.add(this.sun);

    this.resize();

    this.render();
    addEl(this.el);

    this.buttons = createEl('div', { className: 'buttons' });

    addEl(this.el, this.buttons);
    Object.keys(cities).forEach((key) => {
      const button = createEl('button', { innerText: key }, {}, { click: () => { this.search(cities[key]); } });
      addEl(this.buttons, button);
    });

    this.renderer.domElement.addEventListener('dblclick', this.selectLocation);
    window.addEventListener('resize', this.resize);
  }

  selectLocation({ clientX, clientY }) {
    this.mouse.x = (clientX / this.width) * 2 - 1;
    this.mouse.y = -(clientY / this.height) * 2 + 1;
    this.raycaster.setFromCamera(this.mouse, this.camera);
    const intersects = this.raycaster.intersectObject(this.globe, false);
    // console.log(intersects[0].uv);
    if (intersects.length) {
      const { x, y } = intersects[0].uv;
      const lng = x * 360 - 180 + 15; // our globe might be spun by 15 degrees?
      const lat = y * -180 + 90;
      this.search({ lat, lng });
      // const { point } = intersects[0];
      // console.log(point, locationFromPosition(point));
    }
  }

  search(location) {
    console.log('searching places in', location);
    fetch('/api/search', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ location }),
    }).then(res => res.json()).then((res) => {
      console.log(res);
      res.forEach((place) => {
        const marker = new Mesh(new SphereGeometry(0.005, 16, 16), new MeshNormalMaterial());
        const { x, y, z } = positionFromLocation(place.location);
        marker.position.set(x, y, z);
        this.globe.add(marker);
      });
    });
  }

  render() {
    this.controls.update();
    this.controls.rotateSpeed = (this.camera.position.length() - 1) * 0.25;
    // this.scene.fog.far = (this.camera.position.length() - 1);
    // console.log(this.camera.position.length() - 1);// distanceTo(origin))
    this.renderer.render(this.scene, this.camera);
    requestAnimationFrame(this.render);
  }

  resize() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(this.width, this.height);
  }
}

const app = new App();
if (location.hostname === 'localhost') window.app = app;
